<?php

namespace Drupal\entity_auth;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Password\PasswordInterface;

/**
 * Handles key and hash generation and authentication.
 */
class Authentication implements AuthenticationInterface {

  /**
   * The password service.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected $passwordChecker;

  /**
   * UUID generator service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Constructs an Authentication object.
   *
   * @param \Drupal\Core\Password\PasswordInterface $password_checker
   *   The password service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   Drupal UUID generator service.
   */
  public function __construct(PasswordInterface $password_checker, UuidInterface $uuid) {
    $this->passwordChecker = $password_checker;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAuthorizationCode(?int $size, ?string $allowed_characters): string {
    // Fall back to the default code size if not supplied.
    if (!$size) {
      $size = self::VALIDATION_CODE_LENGTH;
    }
    // Fall back to the default code characters if not supplied.
    if (!$allowed_characters) {
      $allowed_characters = self::VALIDATION_CODE_CHARACTERS;
    }
    // Sanitize the code characters string.
    $characters = $this->sanitizeAllowedCharacters($allowed_characters);
    // Get the length of the validation code characters string.
    $validCharNumber = strlen($characters);
    // Initialize the result string.
    $result = '';

    for ($i = 0; $i < $size; $i++) {
      $index = mt_rand(0, $validCharNumber - 1);
      $result .= $characters[$index];
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function generateKey(int $length = 32): string {
    return Crypt::randomBytesBase64($length);
  }

  /**
   * {@inheritdoc}
   */
  public function generateAuthenticationKeySet(string $medium, ?int $code_length, ?string $allowed_characters): AuthenticationKeySet {
    return new AuthenticationKeySet(
      $medium,
      $this->uuid->generate(),
      $this->generateKey(),
      $this->generateAuthorizationCode($code_length, $allowed_characters),
      $this->generateKey(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function generateAuthenticationHashSet(AuthenticationKeySet $key_set): AuthenticationHashSet {
    return new AuthenticationHashSet(
      $key_set->getMedium(),
      $key_set->getId(),
      $this->passwordChecker->hash($key_set->getRouteKey()),
      $this->passwordChecker->hash($key_set->getCodeKey()),
      $this->passwordChecker->hash($key_set->getQueryParameterKey()),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hash(string $key): string {
    return $this->passwordChecker->hash($key);
  }

  /**
   * Sanitizes the allowed characters for code key generation.
   *
   * @param string $characters
   *   A string of the allowed characters.
   *
   * @return string
   *   Returns a string of unique and valid characters.
   */
  private function sanitizeAllowedCharacters(string $characters) {
    // Make sure the allowed characters are unique and valid.
    if (strlen($characters) < 2) {
      throw new \Exception('Cannot generate code with less than two unique allowed characters.');
    }
    // Split the characters into a string to we can check for duplicates.
    $list = str_split($characters, $length = 1);
    // Discard duplicate characters.
    $unique = array_unique($list);

    if (count($unique) < 2) {
      throw new \Exception('Cannot generate code with less than two unique allowed characters.');
    }

    return implode($unique);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(string $key, string $hash) : bool {
    return $this->passwordChecker->check($key, $hash);
  }

}
