<?php

namespace Drupal\entity_auth\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\entity_auth\EntityAuthManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Route;

/**
 * Checks access to entity authentication routes.
 */
class EntityAuthAccessCheck implements AccessInterface {

  /**
   * Entity Authentication Plugin Manager.
   *
   * @var \Drupal\entity_auth\EntityAuthManager
   */
  protected $entityAuthManager;

  /**
   * Symfony Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Entity Authentication Plugin instance.
   *
   * @var \Drupal\entity_auth\EntityAuthInterface
   */
  protected $plugin;

  /**
   * Drupal Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the access check class.
   *
   * @param \Drupal\entity_auth\EntityAuthManager $entity_auth_manager
   *   Entity Authentication Plugin Manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Symfony Request Stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal Entity Type Manager.
   */
  public function __construct(EntityAuthManager $entity_auth_manager, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityAuthManager = $entity_auth_manager;
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Access callback.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route) {
    // Instantiate the plugin from the route defaults.
    $this->plugin = $this->entityAuthManager->createInstance($route->getDefault('plugin_id'));

    if (!$route->hasDefault('entity_type_id')) {
      return AccessResult::forbidden('Missing entity type id');
    }

    if (!$this->requestStack->getCurrentRequest()->attributes->has('entity')) {
      return AccessResult::forbidden('Missing entity id');
    }

    $entity = $this->entityTypeManager->getStorage($route->getDefault('entity_type_id'))->load($this->requestStack->getCurrentRequest()->attributes->get('entity'));

    if (!$entity instanceof EntityInterface) {
      return AccessResult::forbidden('Entity not found');
    }

    return $this->plugin->authenticateRouteKey($entity);
  }

}
