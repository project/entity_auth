<?php

namespace Drupal\entity_auth;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\entity_auth\Event\EntityAuthFinalizeEvent;
use Drupal\entity_auth\Form\EntityAuthForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base class for entity_auth plugins.
 */
abstract class EntityAuthBase extends PluginBase implements EntityAuthInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The password service.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected $passwordChecker;

  /**
   * Custom Authentication Tempstore.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected $tempStore;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Current User service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal Form Builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Key and hash management service.
   *
   * @var \Drupal\entity_auth\AuthenticationInterface
   */
  protected $authentication;

  /**
   * Drupal Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an Authentication object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Password\PasswordInterface $password_checker
   *   The password service.
   * @param \Drupal\entity_auth\AuthTempstoreFactory $auth_tempstore_factory
   *   Custom tempstore factory service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   Drupal Form Builder service.
   * @param \Drupal\entity_auth\AuthenticationInterface $authentication
   *   Key and hash management service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal Entity Type Manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PasswordInterface $password_checker, AuthTempstoreFactory $auth_tempstore_factory, MailManagerInterface $mail_manager, RequestStack $request_stack, EventDispatcherInterface $event_dispatcher, AccountProxyInterface $current_user, FormBuilderInterface $form_builder, AuthenticationInterface $authentication, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->passwordChecker = $password_checker;
    // Custom tempstore using the plugin provider and variable expiration.
    $this->tempStore = $auth_tempstore_factory->getWithCustomExpire($plugin_definition['provider'], $this->getHashExpiration());
    $this->mailManager = $mail_manager;
    $this->requestStack = $request_stack;
    $this->eventDispatcher = $event_dispatcher;
    $this->currentUser = $current_user;
    $this->formBuilder = $form_builder;
    $this->authentication = $authentication;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('password'),
      $container->get('entity_auth.auth_tempstore_factory'),
      $container->get('plugin.manager.mail'),
      $container->get('request_stack'),
      $container->get('event_dispatcher'),
      $container->get('current_user'),
      $container->get('form_builder'),
      $container->get('entity_auth.authentication'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return (string) $this->pluginDefinition['entity_type_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return (string) $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginProvider() : string {
    return $this->pluginDefinition['provider'];
  }

  /**
   * {@inheritdoc}
   */
  public function getHashExpiration(): int {
    return (isset($this->pluginDefinition['hash_expire']) && is_int($this->pluginDefinition['hash_expire'])) ? $this->pluginDefinition['hash_expire'] : 604800;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeKeyLength() : ?int {
    return (isset($this->pluginDefinition['code_length']) && is_int($this->pluginDefinition['code_length'])) ? $this->pluginDefinition['code_length'] : Authentication::VALIDATION_CODE_LENGTH;
  }

  /**
   * {@inheritdoc}
   */
  public function getCodeKeyAllowedCharacters() : ?string {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationRouteId(): string {
    return (string) $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getHashStorageId(EntityInterface $entity): string {
    return (string) $this->getEntityTypeId() . '_' . $this->pluginDefinition['id'] . '_' . $entity->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function destroyAuthenticationHashSet(EntityInterface $entity) {
    $this->tempStore->delete($this->getHashStorageId($entity));
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationHashSet(EntityInterface $entity): ?AuthenticationHashSet {
    return $this->tempStore->get($this->getHashStorageId($entity));
  }

  /**
   * {@inheritdoc}
   */
  public function canCreateAuthenticationSet(EntityInterface $entity, string $medium): bool {
    // All are allowed by default. Plugins can override this (ie: users).
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function createAuthenticationSet(EntityInterface $entity, string $medium): ?AuthenticationKeySet {
    // Leave early if entity not allowed to generate key sets.
    if (!$this->canCreateAuthenticationSet($entity, $medium)) {
      return NULL;
    }
    // Generate a key set object.
    $key_set = $this->authentication->generateAuthenticationKeySet($medium, $this->getCodeKeyLength(), $this->getCodeKeyAllowedCharacters());
    // Save a hash set to the tempstore.
    $this->tempStore->set($this->getHashStorageId($entity), $this->authentication->generateAuthenticationHashSet($key_set));
    // Return the keys.
    return $key_set;
  }

  /**
   * {@inheritdoc}
   */
  public function generateAuthenticationUrl(EntityInterface $entity, AuthenticationKeySet $key_set): Url {
    return Url::fromRoute(
      'entity_auth.' . $this->pluginId,
      [
        'entity' => $entity->id(),
        'key' => $key_set->getRouteKey(),
      ],
      [
        'query' => [
          'auth' => $key_set->getQueryParameterKey(),
        ],
      ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function canAuthenticate(EntityInterface $entity): bool {
    // All are allowed by default. Plugins can override this (ie: users).
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticateRouteKey(EntityInterface $entity): AccessResultInterface {
    // Forbidden entities cannot authenticate.
    if (!$this->canAuthenticate($entity)) {
      return AccessResult::forbidden("Entity forbidden to authenticate.");
    }
    // First look for a key in the route.
    $key = $this->requestStack->getCurrentRequest()->attributes->get('key');

    // If there is no key, 403 them.
    if (!is_string($key)) {
      return AccessResult::forbidden('No key found or key is not of correct Type');
    }

    // Get the hash data from the tempstore.
    $hash_set = $this->getAuthenticationHashSet($entity);
    // Deny access if the hash set does not exist.
    if (!$hash_set instanceof AuthenticationHashSet) {
      return AccessResult::forbidden('Authentication hash data not found.');
    }

    // Authenticate the key against the hash.
    $check = $this->passwordChecker->check($key, $hash_set->getRouteHash());

    if ($check) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden('Invalid route protection key.');
  }

  /**
   * {@inheritdoc}
   */
  public function authenticateQueryParameterKey(EntityInterface $entity): bool {
    // Forbidden entities cannot authenticate.
    if (!$this->canAuthenticate($entity)) {
      return FALSE;
    }
    // Try to get the auth key from the query parameters.
    $auth_key = $this->requestStack->getCurrentRequest()->query->get('auth');
    // If the auth key doesn't exist, fail authentication.
    if (!is_string($auth_key)) {
      return FALSE;
    }
    // Get the hash data from the tempstore.
    $hash_set = $this->getAuthenticationHashSet($entity);
    // If there is no hash data, fail authentication.
    if (!$hash_set instanceof AuthenticationHashSet) {
      return FALSE;
    }

    // Authenticate the auth key against the hash data.
    $check = $this->passwordChecker->check($auth_key, $hash_set->getQueryParameterHash());
    if ($check) {
      return TRUE;
    }

    // Fail by default.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticateCodeKey(EntityInterface $entity, string $key): bool {
    // Forbidden entities cannot authenticate.
    if (!$this->canAuthenticate($entity)) {
      return FALSE;
    }
    // Get the hash data for the user out of the tempstore.
    $hash_set = $this->getAuthenticationHashSet($entity);

    // If there's no hash data, return invalid.
    if (!$hash_set instanceof AuthenticationHashSet) {
      return FALSE;
    }

    // Authenticate the key against the hash.
    if ($this->passwordChecker->check($key, $hash_set->getCodeHash())) {
      return TRUE;
    }

    // Default to invalid.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function sendAuthenticationMessage(EntityInterface $entity, AuthenticationKeySet $key_set) {
    switch ($key_set->getMedium()) {
      case 'email':
        $this->sendAuthenticationEmailMessage($entity, $key_set);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageId(EntityInterface $entity, AuthenticationKeySet $key_set): string {
    return $key_set->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageKey(): string {
    return 'entity_auth_' . $this->getPluginId();
  }

  /**
   * Generates an email subject line for the validation message.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being authenticated.
   *
   * @return string
   *   Returns a string containing the subject line.
   */
  public function getAuthenticationEmailMessageSubject(EntityInterface $entity) : string {
    return 'Your Validation Code and Link';
  }

  /**
   * Sends the user an email containing their authentication keys.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to send the message for.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The keys that the message should contain.
   */
  public function sendAuthenticationEmailMessage(EntityInterface $entity, AuthenticationKeySet $key_set) {
    if (!$entity instanceof UserInterface) {
      $user = $this->getAuthenticationMessageUser($entity);
    }
    else {
      $user = $entity;
    }

    $params = [
      'id' => $this->getMessageId($entity, $key_set),
      'subject_data' => $this->getAuthenticationEmailMessageSubject($user),
      'validation' => [
        'code' => $key_set->getCodeKey(),
        'route' => $this->generateAuthenticationUrl($entity, $key_set)->setAbsolute(TRUE)->toString(),
      ],
    ];
    // Send the email.
    $message = $this->mailManager->mail(
    // Message should be sent from the module that provides the plugin.
      $this->getPluginProvider(),
      $this->getMessageKey(),
      $user->getEmail(),
      $user->language()->getId(),
      // Allow plugins to add additional message params...merge with ours.
      $params + $this->getDefaultMessageParams($entity),
      $user->getEmail(),
      TRUE,
    );

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultMessageParams(EntityInterface $entity): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationFormTitle(): string {
    return (string) $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public function finalizeAuthentication(EntityInterface $entity): Url {
    // Invalidate the keys by destroying the hashes.
    $this->destroyAuthenticationHashSet($entity);
    // Create the finalize event.
    $event = new EntityAuthFinalizeEvent($entity, $this);
    // Fire event to get redirect.
    $event = $this->eventDispatcher->dispatch($event, $event->getId());

    // Save the entity if requested.
    if ($event->getEntitySave()) {
      $event->getEntity()->save();
    }
    // Return the redirect URL object.
    return $event->getRedirect();
  }

  /**
   * {@inheritdoc}
   */
  public function buildAuthenticationForm(EntityInterface $entity): array {
    $form_state = new FormState();
    return $this->formBuilder->buildForm($this->getAuthenticationFormClass(), $form_state);
  }

  /**
   * Returns the form class to use for the entity auth form.
   *
   * @return string
   *   The full namespaced class for the form.
   */
  public function getAuthenticationFormClass() {
    return EntityAuthForm::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationFormUrl(EntityInterface $entity, AuthenticationKeySet $key_set): Url {
    return Url::fromRoute(
      'entity_auth.' . $this->getPluginId(),
      [
        'entity' => $entity->id(),
        'key' => $key_set->getRouteKey(),
      ],
    );
  }

}
