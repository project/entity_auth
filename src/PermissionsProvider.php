<?php

namespace Drupal\entity_auth;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * PermissionsProvider service.
 */
class PermissionsProvider {

  use StringTranslationTrait;

  /**
   * Method description.
   */
  public function refreshKeys() {
    /** @var \Drupal\entity_auth\EntityAuthManager $entity_auth_manager */
    $entity_auth_manager = \Drupal::service('plugin.manager.entity_auth');

    $permissions = [];

    foreach ($entity_auth_manager->getDefinitions() as $definition) {
      $permissions += [
        'entity_auth_refresh_keys_' . $definition['id'] => [
          'title' => $this->t('@plugin_label refresh keys', [
            '@entity_type_id' => $definition['entity_type_id'],
            '@plugin_label' => $definition['label'],
          ]),
          'description' => $this->t('Allow refreshing entity auth keys on @entity_type_id entities using the @plugin_label plugin.', [
            '@entity_type_id' => $definition['entity_type_id'],
            '@plugin_label' => $definition['label'],
          ]),
        ],
      ];
    }

    return $permissions;
  }

}
