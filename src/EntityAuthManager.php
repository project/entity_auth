<?php

namespace Drupal\entity_auth;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * EntityAuth plugin manager.
 */
class EntityAuthManager extends DefaultPluginManager {

  /**
   * Current Route Match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Drupal Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs EntityAuthPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   Drupal Current Route Match service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal Entity Type Manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, CurrentRouteMatch $current_route_match, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct(
      'Plugin/EntityAuth',
      $namespaces,
      $module_handler,
      'Drupal\entity_auth\EntityAuthInterface',
      'Drupal\entity_auth\Annotation\EntityAuth'
    );
    $this->alterInfo('entity_auth_info');
    $this->setCacheBackend($cache_backend, 'entity_auth_plugins');
    $this->currentRouteMatch = $current_route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Handles authenticating query parameter keys during route authentications.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect response upon successful auth, auth form if failure.
   */
  public function authenticate() {
    // Create an instance of the plugin.
    /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
    $instance = $this->createInstance($this->currentRouteMatch->getRouteObject()->getDefault('plugin_id'));
    // Load up the entity.
    $entity = $this->entityTypeManager->getStorage($instance->getEntityTypeId())->load($this->currentRouteMatch->getParameter('entity'));
    // Load the user up out of the route parameter.
    // Authenticate the query parameter if available.
    $check = $instance->authenticateQueryParameterKey($entity);

    // If authenticated, fire finalize method on plugin.
    if ($check) {
      // Fire event fun stuff.
      $redirect = $instance->finalizeAuthentication($entity);
      // Redirect the user to the events redirect value.
      return new RedirectResponse($redirect->toString());
    }
    else {
      // Send the user to the form.
      return $instance->buildAuthenticationForm($entity);
    }
  }

  /**
   * Destroys hash data for all plugins on a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to destroy hashes for.
   */
  public function destroyAllHashes(EntityInterface $entity) {
    foreach ($this->getDefinitions() as $definition) {
      // Create an instance of the plugin.
      /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
      $instance = $this->createInstance($definition['id']);
      // If the plugin handles the entity type, destroy hashes for the entity.
      if ($entity->getEntityTypeId() === $instance->getEntityTypeId()) {
        $instance->destroyAuthenticationHashSet($entity);
      }
    }
  }

}
