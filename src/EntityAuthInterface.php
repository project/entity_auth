<?php

namespace Drupal\entity_auth;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Interface for entity_auth plugins.
 */
interface EntityAuthInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the plugin entity type id.
   *
   * @return string
   *   The entity type id for the plugin.
   */
  public function getEntityTypeId() : string;

  /**
   * Returns the unique plugin id.
   *
   * @return string
   *   The unique plugin id.
   */
  public function getPluginId() : string;

  /**
   * Returns the module id providing the plugin.
   *
   * @return string
   *   The module id.
   */
  public function getPluginProvider() : string;

  /**
   * Time to live for hash data in seconds.
   *
   * By default, this uses Drupal's tempstore.
   *
   * @see \Drupal\Core\TempStore\SharedTempStoreFactory::$expire
   *
   * @return int
   *   Returns the time to live in seconds for hash data.
   */
  public function getHashExpiration() : int;

  /**
   * Gets a custom code key length. If not set, the default should be used.
   *
   * @return int|null
   *   Returns integer if custom plugin chooses custom length, NULL falls back.
   */
  public function getCodeKeyLength() : ?int;

  /**
   * Allows overriding default characters for code key generation.
   *
   * @return string
   *   Returns a string of allowed characters. If null, default will be used.
   */
  public function getCodeKeyAllowedCharacters() : ?string;

  /**
   * Returns the validation route id.
   *
   * Ex: /entity_type_id/{entity}/{validation_route_id}/{key}
   *
   * @return string
   *   The validation route id.
   */
  public function getAuthenticationRouteId() : string;

  /**
   * Generates a unique tempstore id to store hash data for a user.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to generate the unique id for.
   *
   * @return string
   *   Returns a unique storage id string.
   */
  public function getHashStorageId(EntityInterface $entity) : string;

  /**
   * Destroys the hash data for the supplied entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to destroy hash data for.
   */
  public function destroyAuthenticationHashSet(EntityInterface $entity);

  /**
   * Retrieves hash set data for a given entity from the tempstore.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\entity_auth\AuthenticationHashSet|null
   *   Hash data object if exists, NULL otherwise.
   */
  public function getAuthenticationHashSet(EntityInterface $entity) : ?AuthenticationHashSet;

  /**
   * Determines whether an authentication key set can be created for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to create the key set for.
   * @param string $medium
   *   The medium in which the key set will be sent.
   *
   * @return bool
   *   Returns TRUE if key set can be generated for entity, FALSE if not.
   */
  public function canCreateAuthenticationSet(EntityInterface $entity, string $medium) : bool;

  /**
   * Creates new key / hash data, stores hashes, returns keys.
   *
   * This should ALWAYS invalidate / replace previously generated keys.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The user to generate keys for.
   * @param string $medium
   *   The medium the keys will be sent to the user (email, sms, etc.).
   *
   * @return \Drupal\entity_auth\AuthenticationKeySet
   *   Return keys to send to the user.
   */
  public function createAuthenticationSet(EntityInterface $entity, string $medium) : ?AuthenticationKeySet;

  /**
   * Generates a URL string to the entity validation form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to generate the url for.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The key set to include in the url.
   *
   * @return \Drupal\Core\Url
   *   The URL object containing keys to send the user.
   */
  public function generateAuthenticationUrl(EntityInterface $entity, AuthenticationKeySet $key_set) : Url;

  /**
   * Determines if an entity is allowed to authenticate.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity attempting to authenticate.
   *
   * @return bool
   *   Returns TRUE if entity is allowed to authenticate, FALSE otherwise.
   */
  public function canAuthenticate(EntityInterface $entity) : bool;

  /**
   * Checks the validity of a supplied key for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to validate against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Returns an access result for the validation route.
   */
  public function authenticateRouteKey(EntityInterface $entity) : AccessResultInterface;

  /**
   * Authenticates the route key on the entity validation page.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to authenticate against.
   *
   * @return bool
   *   Returns TRUE if authentication is ok, FALSE otherwise.
   */
  public function authenticateQueryParameterKey(EntityInterface $entity): bool;

  /**
   * Authenticates a validation code key for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to validate the key for.
   * @param string $key
   *   The key to validate.
   *
   * @return bool
   *   Returns TRUE if valid, FALSE otherwise.
   */
  public function authenticateCodeKey(EntityInterface $entity, string $key) : bool;

  /**
   * Sends a message to the user containing their authentication keys.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to send the keys on.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   Keys to supply in the message.
   */
  public function sendAuthenticationMessage(EntityInterface $entity, AuthenticationKeySet $key_set);

  /**
   * Allows plugins to set additional message parameters.
   *
   * @return array
   *   Returns an array of message parameters to be merged with auth ones.
   */
  public function getDefaultMessageParams(EntityInterface $entity) : array;

  /**
   * Returns the message key for the plugin for handling overrides.
   *
   * @return string
   *   The unique message key for the plugin.
   */
  public function getMessageKey() : string;

  /**
   * Returns a unique message id for the message being sent to the user.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being authenticated.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The keys being sent to the user.
   *
   * @return string
   *   A unique message id for testing, tracking and logging purposes.
   */
  public function getMessageId(EntityInterface $entity, AuthenticationKeySet $key_set) : string;

  /**
   * Gets the user to send authentication message to.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to derive the user on.
   *
   * @return \Drupal\user\UserInterface
   *   The user to send validation keys to.
   */
  public function getAuthenticationMessageUser(EntityInterface $entity) : UserInterface;

  /**
   * Generates a route title for the validation form.
   *
   * @return string
   *   Route titles must be strings.
   */
  public function getAuthenticationFormTitle() : string;

  /**
   * Finalizes entity validation AFTER user has fully authenticated.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that has validated.
   *
   * @return \Drupal\Core\Url
   *   Returns a URL to redirect the user to.
   */
  public function finalizeAuthentication(EntityInterface $entity) : Url;

  /**
   * Builds a renderable authentication form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The user to build the form for.
   *
   * @return array
   *   Returns a renderable form.
   */
  public function buildAuthenticationForm(EntityInterface $entity) : array;

  /**
   * Returns the URL to the plugin authentication form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for generate the form url for.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   A valid key set to handle the form protection key.
   *
   * @return \Drupal\Core\Url
   *   Returns a URL to the authentication form.
   */
  public function getAuthenticationFormUrl(EntityInterface $entity, AuthenticationKeySet $key_set) : Url;

}
