<?php

namespace Drupal\entity_auth\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\entity_auth\EntityAuthManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Adds validation routes for all enabled plugins.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * User validation plugin manager.
   *
   * @var \Drupal\entity_auth\EntityAuthManager
   */
  protected $entityAuthManager;

  /**
   * Constructs a route event subscriber.
   *
   * @param \Drupal\entity_auth\EntityAuthManager $entity_auth_manager
   *   Entity Auth Plugin Manager.
   */
  public function __construct(EntityAuthManager $entity_auth_manager) {
    $this->entityAuthManager = $entity_auth_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Get all plugin definitions and generate validation routes for them.
    foreach ($this->entityAuthManager->getDefinitions() as $definition) {
      /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
      $instance = $this->entityAuthManager->createInstance($definition['id']);
      $collection->add('entity_auth.' . $definition['id'], new Route(
        '/' . $instance->getEntityTypeId() . '/{entity}/' . $instance->getAuthenticationRouteId() . '/{key}',
        [
          '_controller' => 'plugin.manager.entity_auth::authenticate',
          '_title' => $instance->getAuthenticationFormTitle(),
          'plugin_id' => $definition['id'],
          'entity_type_id' => $instance->getEntityTypeId(),
        ],
        [
          '_entity_auth_access_check' => 'TRUE',
        ],
        [
          'no_cache' => 'TRUE',
          'parameters' => [
            'entity' => [
              'type' => 'entity:' . $instance->getEntityTypeId(),
            ],
          ],
        ]
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
