<?php

namespace Drupal\entity_auth\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\entity_auth\EntityAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form class for entity authentication input.
 */
class EntityAuthForm extends FormBase {

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Authentication Plugin Manager.
   *
   * @var \Drupal\entity_auth\EntityAuthManager
   */
  protected $entityAuthManager;

  /**
   * Drupal Current Route Match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The entity being authenticated on.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Entity Authentication Plugin instance.
   *
   * @var \Drupal\entity_auth\EntityAuthInterface
   */
  protected $plugin;

  /**
   * Constructs the form including.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\entity_auth\EntityAuthManager $entity_auth_plugin_manager
   *   Entity Authentication Plugin Manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   Drupal Current Route Match.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityAuthManager $entity_auth_plugin_manager, CurrentRouteMatch $current_route_match) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityAuthManager = $entity_auth_plugin_manager;
    $this->currentRouteMatch = $current_route_match;
    $this->plugin = $entity_auth_plugin_manager->createInstance($this->currentRouteMatch->getRouteObject()->getDefault('plugin_id'));
    $this->entity = $this->entityTypeManager->getStorage($this->currentRouteMatch->getRouteObject()->getDefault('entity_type_id'))->load($this->currentRouteMatch->getParameter('entity'));

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_auth'),
      $container->get('current_route_match'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->plugin->getPluginDefinition()['provider'] . '__entity_auth__' . $this->plugin->getEntityTypeId() . '__' . $this->plugin->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['instructions_intro'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'text-center',
          'lead',
        ],
      ],
      '#weight' => 300,
    ];

    $form['instructions_intro']['intro'] = [
      '#markup' => $this->t("You're almost there!"),
    ];

    $form['instructions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'user-validation-instructions',
          'text-center',
        ],
      ],
      '#weight' => 400,
    ];

    $form['instructions']['copy'] = [
      '#markup' => $this->t("We have sent you a validation code in your email. Please copy the code here, or click the link in your email to complete your authentication."),
    ];

    $form['validation_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Authorization Code'),
      '#description' => $this->t('You should have received this in your email inbox.'),
      '#size' => 7,
      '#maxlength' => 7,
      '#required' => TRUE,
      '#weight' => 500,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'user-validation-actions',
        ],
      ],
      '#weight' => 1000,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Validate'),
      '#attributes' => [
        'class' => [
          'action',
        ],
      ],
    ];

    $form['actions']['resend'] = [
      '#type' => 'submit',
      '#value' => $this->t('Resend Validation'),
      '#limit_validation_errors' => [],
      '#submit' => ['::resetValidationKeys'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // If route auth fails, we need the validation code to validate.
    // Measure the length of the validation field value.
    if (mb_strlen($form_state->getValue('validation_code')) !== 7) {
      $form_state->setErrorByName('validation_code', $this->t('Invalid code'));
    }

    // Check the validation code for validity.
    $validation_code = (string) $form_state->getValue('validation_code');

    // Authenticate their code.
    if (!$this->plugin->authenticateCodeKey($this->entity, $validation_code)) {
      $form_state->setErrorByName('validation_code', $this->t('Invalid code'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Finalize the user registration.
    $redirect = $this->plugin->finalizeAuthentication($this->entity);

    // Set the form redirect to our new url provided by the finalization.
    $form_state->setRedirectUrl($redirect);
  }

  /**
   * Regenerates and sends new validation keys.
   *
   * This method should add a redirect to the form with the new route protection
   * key supplied by the plugin.
   *
   * @param array $form
   *   This form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function resetValidationKeys(array $form, FormStateInterface $form_state) {
    // @todo Add system for changing authentication mediums.
    $medium = 'email';

    $key_set = $this->plugin->createAuthenticationSet($this->entity, $medium);

    if ($key_set) {
      $this->plugin->sendAuthenticationMessage($this->entity, $key_set);
      // Send the user to the newest validation form.
      $form_state->setRedirectUrl($this->plugin->getAuthenticationFormUrl($this->entity, $key_set));
    }
  }

}
