<?php

namespace Drupal\entity_auth\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\entity_auth\EntityAuthInterface;

/**
 * Event that is fired when entity authentication is successful.
 */
class EntityAuthFinalizeEvent extends Event {

  /**
   * The entity being authenticated.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected EntityInterface $entity;

  /**
   * Plugin instance.
   *
   * @var \Drupal\entity_auth\EntityAuthInterface
   */
  protected $plugin;

  /**
   * The url to redirect the user to upon finalizing registration.
   *
   * @var \Drupal\Core\Url
   */
  protected Url $url;

  /**
   * If set to TRUE, the plugin should save the entity after events.
   *
   * @var bool
   */
  protected bool $entitySave = FALSE;

  /**
   * Constructs event to allow modules to manipulate the user and redirects.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being authenticated.
   * @param \Drupal\entity_auth\EntityAuthInterface $plugin
   *   The instance of the plugin that fired this event.
   */
  public function __construct(EntityInterface $entity, EntityAuthInterface $plugin) {
    $this->entity = $entity;
    $this->plugin = $plugin;
    // Set the default redirect to the home page.
    $this->url = Url::fromRoute('<front>');
  }

  /**
   * Returns a unique event id based on the plugin.
   *
   * @return string
   *   Example: my_module.user_validate.plugin_id.
   */
  public function getId() : string {
    return $this->plugin->getPluginDefinition()['provider'] . '.entity_auth.' . $this->plugin->getPluginId() . '.finalize';
  }

  /**
   * Gets the plugin instance that fired this event.
   *
   * @return \Drupal\entity_auth\EntityAuthInterface
   *   Returns the plugin instance.
   */
  public function getPlugin(): EntityAuthInterface {
    return $this->plugin;
  }

  /**
   * Gets the user that is finalizing their registration.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity() : EntityInterface {
    return $this->entity;
  }

  /**
   * Sets the redirect for the user upon finalizing registration.
   *
   * @param \Drupal\Core\Url $url
   *   A valid internally routed URL object.
   *
   * @return \Drupal\Core\Url
   *   Returns the URL object after setting it.
   */
  public function setRedirect(Url $url) : Url {
    return $this->url = $url;
  }

  /**
   * Gets the URL that the user will be redirected to.
   *
   * @return \Drupal\Core\Url
   *   The URL object if available, NULL otherwise.
   */
  public function getRedirect() : Url {
    return $this->url;
  }

  /**
   * Indicates to the plugin that the entity should be saved.
   *
   * @param bool $entity_save
   *   Set to TRUE to save the entity after event firing.
   *
   * @return bool
   *   Returns the value passed after setting it in class.
   */
  public function setEntitySave(bool $entity_save) : bool {
    return $this->entitySave = $entity_save;
  }

  /**
   * Gets the status of entitySave.
   *
   * @return bool
   *   TRUE if the plugin should save the entity, FALSE otherwise.
   */
  public function getEntitySave() : bool {
    return $this->entitySave;
  }

}
