<?php

namespace Drupal\entity_auth;

/**
 * Defines user registration validation keys. These are never to be stored.
 */
class AuthenticationKeySet {

  /**
   * Unique id for the message sent to the user containing these keys.
   *
   * @var string
   */
  protected string $id;

  /**
   * The medium in which the validation keys are sent (ex: email, sms, etc.).
   *
   * @var string
   */
  protected string $medium;

  /**
   * The key to validate the form route against.
   *
   * @var string
   */
  protected string $routeKey;

  /**
   * The string code for user input validation.
   *
   * @var string
   */
  protected string $codeKey;

  /**
   * The route validation string for automatic validation.
   *
   * @var string
   */
  protected string $queryKey;

  /**
   * Constructs a AuthenticationKeySet object to be passed around.
   *
   * @param string $medium
   *   The medium in which the keys should be sent. (ex: email, sms, etc.).
   * @param string $id
   *   Unique id for this key / hash set.
   * @param string $route_key
   *   The route protection key.
   * @param string $code_key
   *   The randomly generated code for user validation.
   * @param string $query_parameter_key
   *   Query parameter key.
   */
  public function __construct(string $medium, string $id, string $route_key, string $code_key, string $query_parameter_key) {
    $this->medium = $medium;
    $this->id = $id;
    $this->routeKey = $route_key;
    $this->codeKey = $code_key;
    $this->queryKey = $query_parameter_key;
  }

  /**
   * Gets the unique id for the key / hash set.
   *
   * @return string
   *   The id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Returns the medium to send the keys.
   *
   * @return string
   *   Example: email, sms, push_notification, etc.
   */
  public function getMedium() : string {
    return $this->medium;
  }

  /**
   * Returns the route protection key.
   *
   * @return string
   *   The route protection key.
   */
  public function getRouteKey() : string {
    return $this->routeKey;
  }

  /**
   * Returns the query parameter key.
   *
   * @return string
   *   The key to validate the user via a link.
   */
  public function getQueryParameterKey() : string {
    return $this->queryKey;
  }

  /**
   * Returns the randomly generated code for user validation.
   *
   * @return string
   *   Randomly generated code for form input.
   */
  public function getCodeKey() : string {
    return $this->codeKey;
  }

}
