<?php

namespace Drupal\entity_auth\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines entity_auth annotation object.
 *
 * @Annotation
 */
class EntityAuth extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The entity type id used for validation.
   *
   * @var string
   */
  public $entity_type_id;

  /**
   * Hash storage expiration time in seconds.
   *
   * @var int
   */
  public $hash_expire;

}
