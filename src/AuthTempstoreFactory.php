<?php

namespace Drupal\entity_auth;

use Drupal\Core\TempStore\SharedTempStore;
use Drupal\Core\TempStore\SharedTempStoreFactory;

/**
 * Custom extended tempstore factory with overridden expire time.
 */
class AuthTempstoreFactory extends SharedTempStoreFactory {

  /**
   * Creates a shared tempstore with variable expiration.
   *
   * @param string $name
   *   The name of the tempstore collection to get.
   * @param int $expires
   *   The desired expiration time in seconds.
   *
   * @return \Drupal\Core\TempStore\SharedTempStore
   *   Returns a shared tempstore object with new expiration time.
   */
  public function getWithCustomExpire(string $name, int $expires): SharedTempStore {
    $original_expires = $this->expire;
    $this->expire = $expires;
    $storage = $this->get($name);
    $this->expire = $original_expires;
    return $storage;
  }

}
