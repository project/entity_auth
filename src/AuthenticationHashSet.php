<?php

namespace Drupal\entity_auth;

/**
 * Stores validation hashes for user registration.
 */
class AuthenticationHashSet {

  /**
   * Unique id for the key / hash set.
   *
   * @var string
   */
  protected string $id;

  /**
   * The medium in which the keys are sent.
   *
   * These should be verified during validation.
   *
   * @var string
   */
  protected string $medium;

  /**
   * The hash used to protect the route url.
   *
   * @var string
   */
  protected string $queryHash;

  /**
   * The route hash used for securing routes transitions between routes.
   *
   * @var string
   */
  protected string $routeHash;

  /**
   * The hash of the validation code.
   *
   * @var string
   */
  protected string $codeHash;

  /**
   * Constructs the object for use with immediate storage.
   *
   * @param string $medium
   *   The medium in which the keys are being sent (ex: email, sms, etc.)
   * @param string $id
   *   Unique id for the key / hash set.
   * @param string $route_hash
   *   Route protection hash.
   * @param string $code_hash
   *   Validation code hash.
   * @param string $query_hash
   *   Query parameter hash.
   */
  public function __construct(string $medium, string $id, string $route_hash, string $code_hash, string $query_hash) {
    $this->medium = $medium;
    $this->id = $id;
    $this->routeHash = $route_hash;
    $this->codeHash = $code_hash;
    $this->queryHash = $query_hash;
  }

  /**
   * Gets the unique id for the key / hash set.
   *
   * @return string
   *   The id.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Returns the validation code hash.
   *
   * @return string
   *   The hash.
   */
  public function getCodeHash() : string {
    return $this->codeHash;
  }

  /**
   * Returns the form protection hash.
   *
   * @return string
   *   The hash.
   */
  public function getRouteHash() : string {
    return $this->routeHash;
  }

  /**
   * Returns the query parameter hash.
   *
   * @return string
   *   The hash.
   */
  public function getQueryParameterHash() : string {
    return $this->queryHash;
  }

  /**
   * Gets the medium in which the keys were sent. (email, sms, etc.)
   *
   * @return string
   *   The medium in which the keys were sent.
   */
  public function getMedium() : string {
    return $this->medium;
  }

}
