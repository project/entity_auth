<?php

namespace Drupal\entity_auth;

/**
 * Handles key and hash generation and authentication.
 */
interface AuthenticationInterface {

  /**
   * Characters allowed in a validation code.
   */
  const VALIDATION_CODE_CHARACTERS = '123456789ABCDEFGHJKMNPQRSTUXYVWZ';

  /**
   * Default validation code length.
   */
  const VALIDATION_CODE_LENGTH = 7;

  /**
   * Generates an authorization code of a given size.
   *
   * @return string
   *   A string of random numbers and uppercase letters.
   */
  public function generateAuthorizationCode(?int $size, string $allowed_characters): string;

  /**
   * Generates a random cryptographic key.
   *
   * @param int $length
   *   The length in characters of the key.
   *
   * @return string
   *   Returns the key string.
   */
  public function generateKey(int $length = 64) : string;

  /**
   * Generates a random set of keys for authentication.
   *
   * @param string $medium
   *   The medium in which the user will receive keys (email, sms, etc.).
   * @param int|null $code_length
   *   The length of validation code. Null falls back to default.
   * @param string|null $allowed_characters
   *   String of characters to be used for the code key. Null for default.
   *
   * @return \Drupal\entity_auth\AuthenticationKeySet
   *   Returns an object containing a unique id, keys, and medium.
   */
  public function generateAuthenticationKeySet(string $medium, ?int $code_length, ?string $allowed_characters) : AuthenticationKeySet;

  /**
   * Hashes a key set for storage.
   *
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The key set to hash.
   *
   * @return \Drupal\entity_auth\AuthenticationHashSet
   *   A hash set for the supplied keys.
   */
  public function generateAuthenticationHashSet(AuthenticationKeySet $key_set) : AuthenticationHashSet;

  /**
   * Hashes a string.
   *
   * @param string $key
   *   The string to hash.
   *
   * @return string
   *   Returns the hash.
   */
  public function hash(string $key) : string;

  /**
   * Authenticates a key against a hash.
   *
   * @param string $key
   *   The key.
   * @param string $hash
   *   The hash.
   *
   * @return bool
   *   Returns TRUE if authentication passes, FALSE otherwise.
   */
  public function authenticate(string $key, string $hash) : bool;

}
