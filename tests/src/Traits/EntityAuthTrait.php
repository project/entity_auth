<?php

namespace Drupal\Tests\entity_auth\Traits;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\entity_auth\Authentication;
use Drupal\entity_auth\AuthenticationHashSet;
use Drupal\entity_auth\AuthenticationKeySet;

/**
 * Custom Testing Trait to handle User Registration Messages.
 */
trait EntityAuthTrait {

  use AssertMailTrait;

  /**
   * Test user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Gets a captured email by its unique id in message params.
   *
   * @param string $message_id
   *   The unique id of the email message.
   *
   * @return array|null
   *   Returns the captured email message array, or NULL if not exist.
   */
  public function getEmailById(string $message_id): ?array {
    $emails = $this->getMails();

    foreach ($emails as $email) {
      if ($email['params']['id'] === $message_id) {
        return $email;
      }
    }

    return NULL;
  }

  /**
   * Asserts User validation email was sent to a user and returns it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being authenticated.
   * @param string $plugin_id
   *   The plugin id used to send the authentication message.
   * @param string $message_id
   *   The id of the message defined in the message params.
   *
   * @return array
   *   Returns the email message array.
   */
  public function assertEntityAuthEmailIsSent(EntityInterface $entity, string $plugin_id, string $message_id): ?array {
    /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
    $instance = \Drupal::service('plugin.manager.entity_auth')->createInstance($plugin_id);
    // Get the email based on it's id.
    $email = $this->getEmailById($message_id);
    // The email should exist.
    $this->assertNotEmpty($email, 'Entity authentication email sent.');

    // The email should be sent from the plugin provider module.
    $this->assertEquals($instance->getPluginProvider(), $email['module'], 'Message sent plugin provider module.');
    // The email should be the registration email key.
    $this->assertEquals($instance->getMessageKey(), $email['key']);
    // The email should be sent to the user supplied.
    $this->assertEquals($instance->getAuthenticationMessageUser($entity)->getEmail(), $email['to'], 'Message is sent to the correct email.');

    // The email must contain a validation code in the params.
    $this->assertIsString($email['params']['validation']['code'], 'Validation code is a string value.');
    // The validation code must be the default number of characters long.
    $this->assertEquals(Authentication::VALIDATION_CODE_LENGTH, strlen($email['params']['validation']['code']), 'Validation code is 7 characters in length.');
    // The email must contain a valid URL in the validation params.
    $this->assertNotEmpty(filter_var($email['params']['validation']['route'], FILTER_VALIDATE_URL, [FILTER_NULL_ON_FAILURE]), 'Validation URL is a valid URL');

    // Return the email for later test use.
    return $email;
  }

  /**
   * Asserts that an entity auth email was sent containing keys.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to retrieve keys for.
   * @param string $message_id
   *   The unique message id to be found in the messages params array.
   * @param string $plugin_id
   *   The plugin used to send the authentication message.
   *
   * @return \Drupal\entity_auth\AuthenticationKeySet
   *   Returns the entity authentication keys.
   */
  public function assertEntityAuthEmailKeys(EntityInterface $entity, string $message_id, string $plugin_id): AuthenticationKeySet {

    /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
    $instance = \Drupal::service('plugin.manager.entity_auth')->createInstance($plugin_id);

    // Validate that an email message has been sent to the user.
    $email = $this->assertEntityAuthEmailIsSent($entity, $plugin_id, $message_id);

    // Validate we can reconstruct a AuthenticationKeySet object from the email.
    // We need to grab the route hash from the url provided in the email.
    $parsed_route_validation_url = UrlHelper::parse($email['params']['validation']['route']);
    // Make sure the query string exists for the 'auth' key.
    $this->assertIsString($parsed_route_validation_url['query']['auth']);
    // Parse the validation url for its components.
    $current_url = parse_url($parsed_route_validation_url['path']);
    $paths = explode('/', ltrim($current_url['path'], '/'));
    // The form protection key should exist at index 3.
    $this->assertIsString($paths[3]);

    // Validate the keys against the hashes here.
    /** @var \Drupal\entity_auth\AuthenticationHashSet $has_set */
    $hash_set = $instance->getAuthenticationHashSet($entity);

    $this->assertInstanceOf(AuthenticationHashSet::class, $hash_set, 'Authentication hashes exist in tempstore.');

    // Reconstruct the AuthenticationKeySet object for hash verification.
    $key_set = new AuthenticationKeySet($hash_set->getMedium(), $hash_set->getId(), $paths[3], $email['params']['validation']['code'], $parsed_route_validation_url['query']['auth']);

    // Check the code hash.
    $this->assertTrue(\Drupal::service('password')->check($key_set->getCodeKey(), $hash_set->getCodeHash()), 'Validation code hash matches code key in email message');
    // Check the query auth hash.
    $this->assertTrue(\Drupal::service('password')->check($key_set->getQueryParameterKey(), $hash_set->getQueryParameterHash()), 'Query parameter validation hash matches route key in email message.');
    // Check the form protection hash.
    $this->assertTrue(\Drupal::service('password')->check($key_set->getRouteKey(), $hash_set->getRouteHash()), 'Route protection key is correct inside email message to user.');

    // Return the registration keys for later test use.
    return $key_set;
  }

  /**
   * Asserts that the user is on the zeus_user default user validation page.
   *
   * @param string $plugin_id
   *   The plugin used to render the entity auth form page.
   */
  public function assertUserOnDefaultEntityAuthenticationPage(string $plugin_id) {
    /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
    $instance = \Drupal::service('plugin.manager.entity_auth')->createInstance($plugin_id);
    // Get the current url and parse it.
    $current_url = parse_url($this->getSession()->getCurrentUrl());
    $paths = explode('/', ltrim($current_url['path'], '/'));

    // The first value should be /user.
    $this->assertEquals($instance->getEntityTypeId(), $paths[0], 'Current path starts with the entity type id.');
    // The second value should be the user id.
    $this->assertTrue(is_numeric($paths[1]), 'Entity Id found in route');
    // So far we're at /entity_type_id/{entity_id}/validate.
    $this->assertEquals($instance->getAuthenticationRouteId(), $paths[2], 'Authentication route id matches plugin value.');
    // We should see the text code and password form fields.
    $this->assertSession()->fieldExists('validation_code');
  }

  /**
   * Asserts auth hashes have been replaced and returns new ones.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to verify new auth hashes against.
   * @param \Drupal\entity_auth\AuthenticationHashSet $hash_set
   *   The original registration auth object.
   * @param string $plugin_id
   *   The plugin to search for hashes on.
   *
   * @return \Drupal\entity_auth\AuthenticationHashSet
   *   The new authentication hash object that replaced the old one.
   */
  public function assertNewEntityAuthHash(EntityInterface $entity, AuthenticationHashSet $hash_set, string $plugin_id): AuthenticationHashSet {
    /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
    $instance = \Drupal::service('plugin.manager.entity_auth')->createInstance($plugin_id);
    $new_hash_set = $instance->getAuthenticationHashSet($entity);

    // Verify that all the data is new and not the same.
    $this->assertNotEquals($hash_set->getId(), $new_hash_set->getId());
    $this->assertNotEquals($hash_set->getQueryParameterHash(), $new_hash_set->getQueryParameterHash());
    $this->assertNotEquals($hash_set->getCodeHash(), $new_hash_set->getCodeHash());
    $this->assertNotEquals($hash_set->getRouteHash(), $new_hash_set->getRouteHash());

    return $new_hash_set;
  }

  /**
   * Asserts the validity of a key set against a hash set.
   *
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The key set to validate.
   * @param \Drupal\entity_auth\AuthenticationHashSet $hash_set
   *   The hash set to validate the keys against.
   */
  public function assertKeyHashSetValidity(AuthenticationKeySet $key_set, AuthenticationHashSet $hash_set) {
    // The id's should be the same.
    $this->assertEquals($key_set->getId(), $hash_set->getId(), 'ID matches in key / hash set');
    // The medium should be the same.
    $this->assertEquals($key_set->getMedium(), $hash_set->getMedium(), 'Medium matches in key / hash set');

    // Check the code hash.
    $this->assertTrue(\Drupal::service('password')->check($key_set->getCodeKey(), $hash_set->getCodeHash()), 'Validation code authenticates against hash.');
    // Check the query auth hash.
    $this->assertTrue(\Drupal::service('password')->check($key_set->getQueryParameterKey(), $hash_set->getQueryParameterHash()), 'Query parameter validation key matches hash.');
    // Check the form protection hash.
    $this->assertTrue(\Drupal::service('password')->check($key_set->getRouteKey(), $hash_set->getRouteHash()), 'Route protection key matches hash.');
  }

  /**
   * Tests that the key set provided does not authenticate with the hash set.
   *
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   Key set to test.
   * @param \Drupal\entity_auth\AuthenticationHashSet $hash_set
   *   Hash set to test against.
   */
  public function assertKeyHashSetInvalidity(AuthenticationKeySet $key_set, AuthenticationHashSet $hash_set) {
    // The ids should not match.
    $this->assertNotEquals($key_set->getId(), $hash_set->getId(), 'Key Hash set ids do not match.');
    // Check the code hash.
    $this->assertFalse(\Drupal::service('password')->check($key_set->getCodeKey(), $hash_set->getCodeHash()), 'Authentication code does not authenticate');
    // Check the query auth hash.
    $this->assertFalse(\Drupal::service('password')->check($key_set->getQueryParameterKey(), $hash_set->getQueryParameterHash()), 'Query parameter key does not authenticate.');
    // Check the form protection hash.
    $this->assertFalse(\Drupal::service('password')->check($key_set->getRouteKey(), $hash_set->getRouteHash()), 'Route protection key does not authenticate.');
  }

  /**
   * Creates a test user.
   */
  public function createTestUser() {
    // Create a dummy user to test against.
    $this->user = \Drupal::entityTypeManager()->getStorage('user')->create([
      'status' => TRUE,
      'mail' => 'marcus_crassus@rome.com',
      'name' => 'marcus_crassus',
      'pass' => \Drupal::service('password_generator')->generate(32),
    ]);
    // Save the user.
    $this->user->save();
  }

  /**
   * Authenticates an entity by the code method.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to authenticate.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The key set.
   */
  public function authenticateEntityByCode(EntityInterface $entity, AuthenticationKeySet $key_set) {
    $form_values = [
      'validation_code' => $key_set->getCodeKey(),
    ];

    $this->drupalGet($this->plugin->getAuthenticationFormUrl($entity, $key_set)->setAbsolute(TRUE)->toString());

    $this->submitForm($form_values, 'Validate');
  }

  /**
   * Authenticates an entity by the route method.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to authenticate.
   * @param \Drupal\entity_auth\AuthenticationKeySet $key_set
   *   The key set.
   */
  public function authenticateEntityByRoute(EntityInterface $entity, AuthenticationKeySet $key_set) {
    // Get the email sent to the user.
    $email = $this->getEmailById($key_set->getId());

    // Go to the route supplied in the email.
    $this->drupalGet($email['params']['validation']['route']);
    // Make sure we get a 200 back and that it's the right page.
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Asserts that authentication hash data has been deleted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The user to check.
   * @param string $plugin_id
   *   The plugin to verify hash deletion on.
   */
  public function assertAuthenticationHashSetDeleted(EntityInterface $entity, string $plugin_id) {
    /** @var \Drupal\zeus_entity_auth\EntityAuthInterface $instance */
    $instance = \Drupal::service('plugin.manager.entity_auth')->createInstance($plugin_id);
    $this->assertEmpty($instance->getAuthenticationHashSet($entity), 'Authentication hash data has been completely removed from the database.');
  }

}
