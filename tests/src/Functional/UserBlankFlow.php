<?php

namespace Drupal\Tests\entity_auth\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\entity_auth\Traits\EntityAuthTrait;
use Drupal\user\Entity\Role;

/**
 * Handles dummy plugin flow tests.
 */
class UserBlankFlow extends BrowserTestBase {

  use EntityAuthTrait;

  /**
   * The default theme to run tests on.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'entity_auth',
    'entity_auth_test',
  ];

  /**
   * Plugin instance of the user_blank plugin.
   *
   * @var \Drupal\entity_auth\EntityAuthInterface
   */
  protected $plugin;

  /**
   * Tests authentication flow on dummy user plugin.
   */
  public function testAuthenticationCodeFlow() {
    // Initialize the entity auth flow.
    $this->drupalGet(Url::fromRoute('entity_auth_test.user_blank.initialize', [
      'user' => $this->user->id(),
    ]));
    // We should get a 200 and redirected to the auth form.
    $this->assertSession()->statusCodeEquals(200);
    // The user should be on the default auth page.
    $this->assertUserOnDefaultEntityAuthenticationPage($this->plugin->getPluginId());
    // Verify hashes have been created in the tempstore.
    $hash_set = $this->plugin->getAuthenticationHashSet($this->user);

    // Verify the email was sent and get the keys from it.
    $key_set = $this->assertEntityAuthEmailKeys($this->user, $hash_set->getId(), $this->plugin->getPluginId());
    // Post to the authentication form.
    $this->authenticateEntityByCode($this->user, $key_set);

    // Assert the finalize event fired and ran.
    $this->assertFinalizeEvent();
  }

  /**
   * Tests authentication flow on dummy user plugin.
   */
  public function testAuthenticationRouteFlow() {
    // Initialize the entity auth flow.
    $this->drupalGet(Url::fromRoute('entity_auth_test.user_blank.initialize', [
      'user' => $this->user->id(),
    ]));
    // We should get a 200 and redirected to the auth form.
    $this->assertSession()->statusCodeEquals(200);
    // The user should be on the default auth page.
    $this->assertUserOnDefaultEntityAuthenticationPage($this->plugin->getPluginId());
    // Verify hashes have been created in the tempstore.
    $hash_set = $this->plugin->getAuthenticationHashSet($this->user);

    // Verify the email was sent and get the keys from it.
    $key_set = $this->assertEntityAuthEmailKeys($this->user, $hash_set->getId(), $this->plugin->getPluginId());
    // Click the link in the email.
    $this->authenticateEntityByRoute($this->user, $key_set);

    // Assert the finalize event fired and ran.
    $this->assertFinalizeEvent();
  }

  /**
   * Asserts that the finalize event has fired.
   */
  private function assertFinalizeEvent() {
    // The user should be logged in.
    // @todo the parent drupalUserIsLoggedIn method doesn't seem to work...
    // The message should appear from the event subscriber.
    $this->assertSession()->responseContains('The Entity Authentication API has fired the user_blank finalize event.');
    // The user should be on their own account page.
    $this->assertSession()->addressEquals(Url::fromRoute('entity.user.canonical', [
      'user' => $this->user->id(),
    ])->setAbsolute(TRUE)->toString());
  }

  /**
   * Sets up the test.
   */
  public function setUp() : void {
    // Run the parent.
    parent::setUp();

    // Allow the anonymous role to view content.
    $this->grantPermissions(Role::load('anonymous'), ['access content']);

    // Create a user to authenticate with.
    $this->createTestUser();

    // Create an instance of the user_blank plugin.
    $this->plugin = $this->container->get('plugin.manager.entity_auth')->createInstance('user_blank');
  }

}
