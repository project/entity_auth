<?php

namespace Drupal\Tests\entity_auth\Kernel;

use Drupal\entity_auth\Authentication;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\entity_auth\Traits\EntityAuthTrait;

/**
 * Tests our custom user registration service.
 *
 * @group zeus_user
 */
class AuthenticationServiceTest extends EntityKernelTestBase {

  use EntityAuthTrait;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'entity_auth',
    'system',
  ];

  /**
   * Custom User entity storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * The password service.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected $passwordService;

  /**
   * Custom Tempstore Factory.
   *
   * @var \Drupal\entity_auth\AuthTempstoreFactory
   */
  protected $tempstoreFactory;

  /**
   * Symfony Request Stack.
   *
   * @var object|\Symfony\Component\HttpFoundation\RequestStack|null
   */
  protected $requestStack;

  /**
   * Key and hash management service.
   *
   * @var \Drupal\entity_auth\AuthenticationInterface
   */
  protected $authentication;

  /**
   * Tests authorization code generator.
   *
   * The generator should always generate valid codes even after trimming.
   *
   * @covers \Drupal\entity_auth\Authentication::generateAuthorizationCode
   * @covers \Drupal\entity_auth\Authentication::hash
   */
  public function testAuthorizationCodeGeneration() {
    $valid_characters = Authentication::VALIDATION_CODE_CHARACTERS;
    // Generate 10,000 random registration codes to test.
    $iterations = 10000;

    for ($x = 0; $x <= $iterations; $x++) {
      // Create a random length code from 1 to 64.
      $length = rand(1, 64);
      // Get the validation code from the registration service.
      $validation_code = $this->authentication->generateAuthorizationCode($length, $valid_characters);
      // Assert that the validation code method returns a string value.
      $this->assertIsString($validation_code, 'Validation code returned as string.');
      // The validation code should be the correct length.
      $this->assertTrue(strlen($validation_code) === $length, 'Initial validation code is correct length.');
      // Forms trim validation codes. We should test for that.
      $trimmed = trim($validation_code);
      // The code should still be a string.
      $this->assertIsString($trimmed);
      // The length should still be correct after trimming.
      $this->assertTrue(strlen($trimmed) === $length, 'Trimmed validation code is the correct length.');
      // Assert that each character is valid in the code.
      $this->assertTrue(!preg_match("/[^$valid_characters]/", $validation_code), 'Validation code only contains valid characters.');
    }
  }

  /**
   * Tests the Key / Hash generator and validates them.
   *
   * @covers \Drupal\entity_auth\Authentication::generateKey
   * @covers \Drupal\entity_auth\Authentication::hash
   */
  public function testKeyHashGenerationAndValidity() {
    // Generate 100 key/hash sets to validate.
    $iterations = 100;
    for ($x = 0; $x <= $iterations; $x++) {
      // Generate a key/hash using the default parameters.
      $key = $this->authentication->generateKey();
      // Validate the key / hash against the password service.
      $this->assertTrue($this->passwordService->check($key, $this->authentication->hash($key)), 'Automatic key generation key / hash set is valid.');
    }

    // Test our own custom key against it.
    $custom_key = 'sooooopersecretkey';
    $this->assertTrue($this->passwordService->check($custom_key, $this->authentication->hash($custom_key)), 'Custom key validates against password service.');

    // Test key lengths from 1 to 64.
    $custom_key_max_length = 64;
    foreach (range(1, $custom_key_max_length) as $key_length) {
      // Generate a key / hash set.
      $key = $this->authentication->generateKey($key_length);
      // Check the values.
      $this->assertTrue($this->passwordService->check($key, $this->authentication->hash($key)));
    }
  }

  /**
   * {@inheritdoc}
   *
   * Set up the test with some required services.
   */
  public function setUp() : void {
    parent::setUp();

    // Set up the user storage service in the test class.
    $this->userStorage = $this->entityTypeManager->getStorage('user');

    // Set up the password service to check key/hash values.
    $this->passwordService = $this->container->get('password');

    // Set up the custom overridden auth tempstore service.
    $this->tempstoreFactory = $this->container->get('entity_auth.auth_tempstore_factory');

    // Set up the request stack.
    $this->requestStack = $this->container->get('request_stack');

    // Set up the authentication service.
    $this->authentication = $this->container->get('entity_auth.authentication');
  }

}
