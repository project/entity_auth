<?php

namespace Drupal\Tests\entity_auth\Kernel;

use Drupal\entity_auth\AuthenticationHashSet;
use Drupal\entity_auth\AuthenticationKeySet;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\Tests\entity_auth\Traits\EntityAuthTrait;
use Drupal\user\UserInterface;

/**
 * Tests the user plugin module.
 *
 * @package Drupal\Tests\entity_auth\Kernel
 */
class UserBlank extends EntityKernelTestBase {

  use EntityAuthTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'entity_auth',
    'entity_auth_test',
  ];

  /**
   * The user_blank plugin instance.
   *
   * @var \Drupal\entity_auth\EntityAuthInterface
   */
  public $plugin;

  /**
   * Entity Auth Tempstore.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  public $tempStore;

  /**
   * Tests the entity id return string.
   *
   * @covers \Drupal\entity_auth\EntityAuthBase::getEntityTypeId
   */
  public function testPluginConfiguration() {
    // Test the plugin id method.
    $this->assertEquals('user_blank', $this->plugin->getPluginId(), 'Proper plugin id returned from method.');
    // Test the label method.
    $this->assertEquals('User Plugin Test', $this->plugin->label(), 'Label matches annotation.');

    // This should come back with the 'user' entity type id.
    $this->assertEquals('user', $this->plugin->getEntityTypeId(), 'Entity type id matches annotation in dummy plugin.');

    // Test the plugin provider method.
    $this->assertEquals('entity_auth_test', $this->plugin->getPluginProvider(), 'Plugin provider matches module name.');

    // Test the custom hash expiration.
    $this->assertIsInt($this->plugin->getHashExpiration(), 'Hash expiration returns integer.');
    $this->assertEquals(604800, $this->plugin->getHashExpiration(), 'Hash expiration is expected default value.');

    // The authentication route id should be the plugin id.
    $this->assertEquals('user_blank', $this->plugin->getAuthenticationRouteId(), 'Authentication route id matches plugin id');

    // The user should be returned for messages.
    $this->assertInstanceOf(UserInterface::class, $this->plugin->getAuthenticationMessageUser($this->user), 'User is returned for messages.');
  }

  /**
   * Tests hash storage functions.
   */
  public function testHashStorage() {
    // Verify the expected hash storage id in the tempstore.
    $this->assertEquals('user_user_blank_' . $this->user->uuid(), $this->plugin->getHashStorageId($this->user), 'Hash storage id matches expected value');

    // Create a new authentication key / hash set.
    $key_set = $this->plugin->createAuthenticationSet($this->user, 'email');

    // The key set should be an instance of the key class.
    $this->assertInstanceOf(AuthenticationKeySet::class, $key_set, 'Key set class returned from plugin createAuthenticationSet method.');
    // Get the hash set from the plugin storage.
    $hash_set = $this->plugin->getAuthenticationHashSet($this->user);
    // Verify that the data exists in the database.
    $this->assertInstanceOf(AuthenticationHashSet::class, $hash_set, 'Hash set exists in database.');

    // Test the authenticity of the key set against the hash.
    $this->assertKeyHashSetValidity($key_set, $hash_set);

    // Remove the hash set from memory.
    unset($hash_set);

    // Destroy the hash set in the database.
    $this->plugin->destroyAuthenticationHashSet($this->user);

    // Try to retrieve the hash set after destruction.
    $hash_set = $this->plugin->getAuthenticationHashSet($this->user);
    // Assert that the hash set has been destroyed.
    $this->assertNull($hash_set, 'Hash set destroyed in database properly.');

    // Remove the key set from memory.
    unset($key_set);
  }

  /**
   * Tests the invalidation of previous keys by generating new ones.
   */
  public function testKeySetInvalidation() {
    // Test the replacement of hash data...hence invaliding keys.
    $key_set = $this->plugin->createAuthenticationSet($this->user, 'email');
    // The key set should be the correct class.
    $this->assertInstanceOf(AuthenticationKeySet::class, $key_set, 'Key set returned from plugin is valid key class.');

    // Get the first hash set and validate it.
    $hash_set = $this->plugin->getAuthenticationHashSet($this->user);
    // The hash set should be the correct class.
    $this->assertInstanceOf(AuthenticationHashSet::class, $hash_set, 'Hash set retrieved from the plugin is the correct class.');

    // Validate the key set against the hash.
    $this->assertKeyHashSetValidity($key_set, $hash_set);

    // Generate new keys, this should invalidate the previous ones.
    $new_key_set = $this->plugin->createAuthenticationSet($this->user, 'email');
    // Assert new hashes have been generated.
    $new_hash_set = $this->assertNewEntityAuthHash($this->user, $hash_set, $this->plugin->getPluginId());
    // Test the old keys against the new hashes.
    $this->assertKeyHashSetInvalidity($key_set, $new_hash_set);

    // Test the new keys against the new hashes.
    $this->assertKeyHashSetValidity($new_key_set, $new_hash_set);
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();

    // Set up an instance of the dummy user plugin.
    $this->plugin = $this->container->get('plugin.manager.entity_auth')->createInstance('user_blank');

    // Load up the custom tempstore to validate values.
    $this->tempStore = $this->container->get('entity_auth.auth_tempstore_factory')->get('entity_auth_test');

    // Create the test user.
    $this->createTestUser();
  }

}
