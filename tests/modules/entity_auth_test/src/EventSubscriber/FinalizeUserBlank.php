<?php

namespace Drupal\entity_auth_test\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity_auth\Event\EntityAuthFinalizeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * User event subscriber.
 */
class FinalizeUserBlank implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Drupal Messenger Service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs the event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal Messenger Service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'entity_auth_test.entity_auth.user_blank.finalize' => [
        // Set the redirect.
        ['setRedirect', 9999],
        // Login the user.
        ['loginUser', 500],
        // Set a message to the screen.
        ['setMessage', 0],
      ],
    ];
  }

  /**
   * Handles setting the redirect on users.
   *
   * @param \Drupal\entity_auth\Event\EntityAuthFinalizeEvent $event
   *   The event.
   */
  public function setRedirect(EntityAuthFinalizeEvent $event) {
    // Send the user to their account page by default.
    $event->setRedirect(Url::fromRoute('user.page'));
  }

  /**
   * Logs the user in.
   *
   * @param \Drupal\entity_auth\Event\EntityAuthFinalizeEvent $event
   *   The event.
   */
  public function loginUser(EntityAuthFinalizeEvent $event) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $event->getEntity();
    user_login_finalize($user);
  }

  /**
   * Displays a message to the user regarding the event.
   *
   * @param \Drupal\entity_auth\Event\EntityAuthFinalizeEvent $event
   *   The event.
   */
  public function setMessage(EntityAuthFinalizeEvent $event) {
    $this->messenger->addMessage($this->t('The Entity Authentication API has fired the @plugin_id finalize event.', [
      '@plugin_id' => $event->getPlugin()->getPluginId(),
    ]));
  }

}
