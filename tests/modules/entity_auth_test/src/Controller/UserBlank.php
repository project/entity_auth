<?php

namespace Drupal\entity_auth_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\entity_auth\EntityAuthManager;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Entity Authentication API Test routes.
 */
class UserBlank extends ControllerBase {

  /**
   * The plugin.manager.entity_auth service.
   *
   * @var \Drupal\entity_auth\EntityAuthManager
   */
  protected $entityAuthManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\entity_auth\EntityAuthManager $plugin_manager_entity_auth
   *   The plugin.manager.entity_auth service.
   */
  public function __construct(EntityAuthManager $plugin_manager_entity_auth) {
    $this->entityAuthManager = $plugin_manager_entity_auth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_auth')
    );
  }

  /**
   * Initializes the dummy auth request.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity to authenticate.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects to the auth form.
   */
  public function initialize(UserInterface $user) {
    // Create an instance of the dummy plugin.
    /** @var \Drupal\entity_auth\EntityAuthInterface $instance */
    $instance = $this->entityAuthManager->createInstance('user_blank');

    // Create keys.
    $key_set = $instance->createAuthenticationSet($user, 'email');

    // Send the email to the user.
    $instance->sendAuthenticationMessage($user, $key_set);

    // Redirect the user to the auth form.
    return new RedirectResponse($instance->getAuthenticationFormUrl($user, $key_set)->toString());
  }

}
