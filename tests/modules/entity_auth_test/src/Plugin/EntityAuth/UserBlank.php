<?php

namespace Drupal\entity_auth_test\Plugin\EntityAuth;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_auth\EntityAuthBase;
use Drupal\user\UserInterface;

/**
 * Plugin implementation of the user_validation.
 *
 * @EntityAuth (
 *   id = "user_blank",
 *   label = @Translation("User Plugin Test"),
 *   description = @Translation("Dummy plugin for testing a mostly blank plugin class."),
 *   entity_type_id = "user"
 * )
 */
class UserBlank extends EntityAuthBase {

  /**
   * {@inheritdoc}
   */
  public function getAuthenticationMessageUser(EntityInterface $entity): UserInterface {
    return $entity;
  }

}
