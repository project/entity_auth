## Entity Auth ##
This module provides a plug-able framework for authenticating users against any
entity. This module does not do anything on it's own. It does, however, provide
a fairly sane default plugin base that does a lot of the heavy lifting. By
default, the module will fire an event to notify subscribers that a user has
passed authentication, ask for a redirect from the subscribers, and move the
user on.

Example use cases:
 - Password-less logins
 - Custom password reset flow
 - One-time access to entities.
 - User registration validation
 - Two-Factor Authentication
